class Sky{
    constructor() {
        this.posX = random(0, width);
        this.posY = random(0, height-80);
        this.speed = random(0,2, 2);
    }
    move(){
        this.posX += this.speed;
        if (this.posX > width){
            this.posX = 0;
        }
    }
    show() {
        fill(255);
        noStroke();
        textSize(50);
        text("☁︎",this.posX, this.posY);
    }
}




 



