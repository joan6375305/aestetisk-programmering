## MiniX6
##### Hvordan virker spillet?
Mit spil fungere ved at bruge tasterne, så du kan nå den anden side af vejen. Med tasterne kan du bevæge bilen frem og tilbage (fra side til side) på en vej. Over vejen ses der nogle skyer, disse skal undgås ellers slutter spillet. Mit spil har taget inspiration fra tomatogame, som vi talte om i undervisningen. Jeg havde ikke en ide til et andet slags spil, da jeg synes opgaven var virkelig svær. Det var derfor vigtigt for mig at lave et spil, som var mit eget. Jeg har brugt meget tid på at få husene til at ligne huse, og omgivelserne til at være flotte. Der skulle have været tilføjet flere funktioner til mit spil, men jeg havde problemer med at få det til at fungere.

##### Spillets roller
En af de funktioner jeg gerne ville have tilføjet til mit spil var, at spilleren ville nå at "vinde" når de kom over til den anden side. Eventuelt ville der dukke en tekst op , som sagde "VINDER!!" eller noget lignende. Jeg havde forsøgt mig frem med nogle `if-statements`, men fik det aldrig det rigtigt at virkelig. Jeg valgte derfor at fjerne funktionen, da jeg ikke ville aflevere et ikke-færdigt program. Udover det har jeg taget inspiration fra tomatogame, som vi gennemgik i undervisningen. Jeg har derfor også opdelt mine filer i `bilspil`, som hele spillet. `Sky`, som de objekter man skal undgå i spillet. `Player`, som er bilen der kan bevæges af en, som spiller spillet. Alle disse filer er tilgængelige her i opgaven. Jeg synes det har været svært at forklare hvad de forskellige objekter har af rolle, og noget nemmere at vise det. 

````
  show() {
        fill(255);
        noStroke();
        textSize(50);
        text("☁︎",this.posX, this.posY);
    }
`````
Ovenstående kode beskriver hvordan min sky skal se ud. Jeg har anvendt en class, som har brug for noget mere specifikt viden om skyen. Jeg har, som de fleste andre, fået skyen til at bevæge sig, bestemt størrelse, placering, m.m. Denne sky skal ses som en del af spillet, selvom den er fjernet fra det store dokument. Jeg har lidt set på denne fil, som fjenden. Det er den som gerne vil standse spillet, og sørger for at spilleren ikke vinder. Hvor `Player` filen er "the hero", som vi gerne vil have til at vinde spillet. Objekterne er essentielle for spillets funktion, men kan klassificeres som noget andet, og er derfor lavet til filer. Da selve `bilspil`-filen handler mere om hvordan det hele ser ud, og hvordan det kobles sammen. Det er nok meget en kringlet forklaring, men jeg håber det giver mening.

##### Ugens læsning
Ugens læsning tror jeg ikke jeg fuldstændig forstod. Det jeg har haft primært fokus på er det, som jeg mener, at jeg kunne relatere til kodning. Specielt meget i forhold til OOP. Her har jeg også et citat med, citatet blev også gennemgået til instruktør. ***"vid Reinfurt “It’s also worth reiterating that OOP is designed to reflect the way the world is organized and imagined, at least from the computer programmers’ perspective.” (Soon & Cox,2020, p. 160)!"***. Det giver mening at opdele og sortere programmering efter objekter. Det giver mening at class beskriver noget overordnet, hvor der indenunder det beskrives noget mere specifikt til selve objektet. Jeg synes opdelingen giver god mening, og jeg kan måske godt se hvordan der kan drages paralleller med den virkelige verden. 

##### Abstraction
Når man ser mit program, ser det meget simpelt ud. Men det er nok fordi jeg har opdelt filerne på den måde som jeg har. Her er tale om abstraction, som vi også gjorde kendskab til under ugens læsning. Vi mennesker er meget simple dyr. Vi kan ikke håndtere særlig meget forskellig information på samme tid, da vi vil føle at det er uoverskueligt. Selvom min kode egentlig indeholder samme information, om opdelt eller ej, vil den ene være uoverkuelig og den anden være simpel. Mennesket gør meget brug af abstractions i mange tilfælde i den virkelige verden. For det handler bare om at opdele information, og for-simple ting. Nogle gange handler det også om at gemme de mindre vigtige detaljer væk. Men uanset kan dette relateres til flere menneskers hverdag. De fleste har en rutine eller en dagligdag som egentlig er delt op. Eksempelvis også hvis man er nede og træne deler man typisk op i forskellige "kropsdele". Det ville være mærkeligt at lave en enkelt pull-up og så en enkelt squad. Men det ændre egentlig ikke på hvor meget du laver, bare fordi rækkefølgen er anderledes. På den måde deler vi forskellige ting op, så vi kan få vores informationer/rutiner/deadline/m.m til at virke mere overskuelige. 

#### Link til mit program
[Klik her for at køre min sketch](https://joan6375305.gitlab.io/aestetisk-programmering/MiniX6/index.html)

- ["Player" source code](https://gitlab.com/joan6375305/aestetisk-programmering/-/blob/main/MiniX6/Player.js)
- ["Sky" source code](https://gitlab.com/joan6375305/aestetisk-programmering/-/blob/main/MiniX6/Sky.js)
- [Klik her for min source code](https://gitlab.com/joan6375305/aestetisk-programmering/-/blob/main/MiniX6/bilspil.js)

### Billeder af mit program
![](MiniX6.png)
![](MiniX66.png)

### Litteraturliste
- [https://p5js.org/reference/](https://p5js.org/reference/)
- [https://stackoverflow.com](https://stackoverflow.com/questions/58907382/how-to-set-the-background-of-an-ellipse-in-javascript-to-an-image)
- [https://emojicombos.com/cloud](https://emojicombos.com/cloud)
