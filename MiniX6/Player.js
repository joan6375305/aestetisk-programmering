class Player {
  constructor(){
      this.posX = width/2;
      this.posY = height-50;
      this.speed = 20;
      this.size = 40;
      this.image = null; 
  }
  preload() {
    this.image = loadImage("bop.png");
  }
  moveUp(){
      this.posY -= this.speed;
  }
  moveDown(){
      this.posY += this.speed;
  }
  moveRight(){
    this.posX += this.speed;
}
moveLeft(){
    this.posX -= this.speed;
}

  show(){
    if (this.image){
        push(); 
        translate(this.posX, this.posY); 
        rotate(HALF_PI); 
        imageMode(CENTER);
        image(this.image, 0, -25, this.image.width/4, this.image.height/4); 
        pop(); 
  }
}}




 



