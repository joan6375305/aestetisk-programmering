let sky = [];
let player;
let antal = 50;

function setup() {
    createCanvas(windowWidth, windowHeight);
    background(50,205,0);
    for (let i = 0; i < antal; i++) {
        sky.push(new Sky());
    }
    player = new Player();
    player.preload();
}

function draw() {
    background(50,205,0);
    //græs
    noStroke();
    fill(173,255,47);
    rect(0,0,1500,10);
    rect(0,20,1500,10);
    rect(0,40,1500,10);
    rect(0,60,1500,10);
    rect(0,80,1500,10);
    rect(0,100,1500,10);
    rect(0,120,1500,10);
    rect(0,140,1500,10);
    rect(0,160,1500,10);
    rect(0,180,1500,10);
    rect(0,200,1500,10);
    rect(0,220,1500,10);
    rect(0,240,1500,10);
    rect(0,260,1500,10);
    rect(0,280,1500,10);
    rect(0,300,1500,10);
    rect(0,320,1500,10);
    rect(0,340,1500,10);
    rect(0,360,1500,10);
    rect(0,380,1500,10);
    rect(0,400,1500,10);
    rect(0,420,1500,10);
    rect(0,440,1500,10);
    rect(0,460,1500,10);
    rect(0,480,1500,10);
    rect(0,500,1500,10);
    rect(0,520,1500,10);
    rect(0,540,1500,10);
    rect(0,560,1500,10);
    rect(0,580,1500,10);
    rect(0,600,1500,10);
    rect(0,620,1500,10);
    rect(0,640,1500,10);
    rect(0,660,1500,10);
    rect(0,680,1500,10);
    rect(0,700,1500,10);
    rect(0,720,1500,10);
    rect(0,740,1500,10);
    rect(0,760,1500,10);
    rect(0,780,1500,10);
    fill(0,128,0);
    rect(0,0,10,1000);
    rect(20,0,10,1000);
    rect(40,0,10,1000);
    rect(60,0,10,1000);
    rect(80,0,10,1000);
    rect(100,0,10,1000);
    rect(120,0,10,1000);
    rect(140,0,10,1000);
    rect(160,0,10,1000);
    rect(180,0,10,1000);
    rect(200,0,10,1000);
    rect(220,0,10,1000);
    rect(240,0,10,1000);
    rect(260,0,10,1000);
    rect(280,0,10,1000);
    rect(300,0,10,1000);
    rect(320,0,10,1000);
    rect(340,0,10,1000);
    rect(360,0,10,1000);
    rect(380,0,10,1000);
    rect(400,0,10,1000);
    rect(420,0,10,1000);
    rect(440,0,10,1000);
    rect(460,0,10,1000);
    rect(480,0,10,1000);
    rect(500,0,10,1000);
    rect(520,0,10,1000);
    rect(540,0,10,1000);
    rect(560,0,10,1000);
    rect(580,0,10,1000);
    rect(600,0,10,1000);
    rect(620,0,10,1000);
    rect(640,0,10,1000);
    rect(660,0,10,1000);
    rect(680,0,10,1000);
    rect(700,0,10,1000);
    rect(720,0,10,1000);
    rect(740,0,10,1000);
    rect(760,0,10,1000);
    rect(780,0,10,1000);
    rect(800,0,10,1000);
    rect(820,0,10,1000);
    rect(840,0,10,1000);
    rect(860,0,10,1000);
    rect(880,0,10,1000);
    rect(900,0,10,1000);
    rect(920,0,10,1000);
    rect(940,0,10,1000);
    rect(960,0,10,1000);
    rect(980,0,10,1000);
    rect(1000,0,10,1000);
    rect(1020,0,10,1000);
    rect(1040,0,10,1000);
    rect(1060,0,10,1000);
    rect(1080,0,10,1000);
    rect(1100,0,10,1000);
    rect(1120,0,10,1000);
    rect(1140,0,10,1000);
    rect(1160,0,10,1000);
    rect(1180,0,10,1000);
    rect(1200,0,10,1000);
    rect(1220,0,10,1000);
    rect(1240,0,10,1000);
    rect(1260,0,10,1000);
    rect(1280,0,10,1000);
    rect(1300,0,10,1000);
    rect(1320,0,10,1000);
    rect(1340,0,10,1000);
    rect(1360,0,10,1000);
    rect(1380,0,10,1000);
    rect(1400,0,10,1000);
    rect(1420,0,10,1000);
    rect(1440,0,10,1000);
    rect(1460,0,10,1000);
    rect(1480,0,10,1000);
    rect(1500,0,10,1000);

    //vejen
    fill(0);
    rect(710,0,100,800);
    fill(255,255,0);
    rect(755,20,10,30);
    rect(755,80,10,30);
    rect(755,140,10,30);
    rect(755,200,10,30);
    rect(755,260,10,30);
    rect(755,320,10,30);
    rect(755,380,10,30);
    rect(755,440,10,30);
    rect(755,500,10,30);
    rect(755,560,10,30);
    rect(755,620,10,30);
    rect(755,680,10,30);

    //hus 1
    noStroke();
    fill(255,69,0);
    rect(500,20,100,100);
    rect(600,45,40,50);
    fill(218,165,32);
    rect(640,55,70,30);
    stroke(255,0,0);
    line(600, 120, 500, 20);
    line(500, 120, 600, 20);

    //hus 2
    noStroke();
    fill(30,144,255);
    rect(500,400,100,200);
    rect(600,525,40,50);
    fill(139,69,19);
    rect(640,535,70,30);
    stroke(0,0,255);
    line(600, 400, 500, 600);
    line(600, 600, 500, 400);

    //hus 3
    noStroke();
    fill(255,105,180);
    rect(900,70,100,200);
    rect(860,85,40,50);
    rect(860,205,40,50);
    fill(244,164,96);
    rect(810,95,50,30);
    rect(810,215,50,30);
    stroke(255,20,147);
    line(900, 270, 1000, 70);
    line(1000, 270, 900, 70);

    //hus 4
    noStroke();
    fill(160,160,160);
    rect(900,470,100,130);
    rect(860,485,40,50);
    fill(210,105,30);
    rect(810,495,50,30);
    stroke(96);
    line(900, 600, 1000, 470);
    line(1000, 600, 900, 470);

    //vejen
    noStroke();
    fill(0);
    rect(710,0,100,800);
    fill(255,255,0);
    rect(755,20,10,30);
    rect(755,80,10,30);
    rect(755,140,10,30);
    rect(755,200,10,30);
    rect(755,260,10,30);
    rect(755,320,10,30);
    rect(755,380,10,30);
    rect(755,440,10,30);
    rect(755,500,10,30);
    rect(755,560,10,30);
    rect(755,620,10,30);
    rect(755,680,10,30);

    //hus 1
    noStroke();
    fill(255,69,0);
    rect(500,20,100,100);
    rect(600,45,40,50);
    fill(218,165,32);
    rect(640,55,70,30);
    stroke(255,0,0);
    line(600, 120, 500, 20);
    line(500, 120, 600, 20);
    line(639, 70, 600, 70);

    //hus 2
    noStroke();
    fill(30,144,255);
    rect(500,400,100,200);
    rect(600,525,40,50);
    fill(139,69,19);
    rect(640,535,70,30);
    stroke(0,0,255);
    line(600, 400, 500, 600);
    line(600, 600, 500, 400);
    line(639, 550, 600, 550);

    //hus 3
    noStroke();
    fill(255,105,180);
    rect(900,70,100,200);
    rect(860,85,40,50);
    rect(860,205,40,50);
    fill(244,164,96);
    rect(810,95,50,30);
    rect(810,215,50,30);
    stroke(255,20,147);
    line(900, 270, 1000, 70);
    line(1000, 270, 900, 70);
    line(900, 230, 860, 230);
    line(900, 110, 860, 110);

    //hus 4
    noStroke();
    fill(160,160,160);
    rect(900,470,100,130);
    rect(860,485,40,50);
    fill(210,105,30);
    noStroke();
    rect(810,495,50,30);
    stroke(96);
    line(900, 600, 1000, 470);
    line(1000, 600, 900, 470);
    line(900, 510, 860, 510);

    //natur
    noStroke();
    fill(100,149,237);
    ellipse(600,160,40);
    ellipse(630,145,40);
    ellipse(620,160,40);
    ellipse(900,440,40);
    ellipse(930,425,40);
    ellipse(920,440,40);
    ellipse(940,440,40);
    fill(0,100,0);
    ellipse(500,200,30);
    ellipse(540,180,25);
    fill(154,205,50);
    ellipse(540,220,35);
    ellipse(480,150,35);
    ellipse(650,20,30)
    fill(0,100,0);
    ellipse(500,370,30);
    ellipse(540,380,25);
    fill(154,205,50);
    ellipse(540,670,35);
    ellipse(460,500,35);
    fill(0,100,0);
    ellipse(650,500,30)
    fill(0,100,0);
    ellipse(900,370,30);
    ellipse(1040,380,25);
    fill(154,205,50);
    ellipse(1040,670,35);
    ellipse(1060,500,35);
    fill(0,100,0);
    ellipse(1050,600,30)
    ellipse(850,680,30)
    fill(0,100,0);
    ellipse(1020,200,30);
    ellipse(840,180,25);
    fill(154,205,50);
    ellipse(1040,240,35);
    ellipse(980,40,35);
    ellipse(850,20,30);

    spawnSky();
    player.show();
    checkCollision();
}

function spawnSky() {
    for (let i = 0; i < sky.length; i++) {
        sky[i].move();
        sky[i].show();
    }
}

function keyPressed() {
    if (keyCode === UP_ARROW) {
        player.moveUp();
    }
    else if (keyCode === DOWN_ARROW) {
        player.moveDown();
    }
    else if (keyCode === RIGHT_ARROW) {
        player.moveRight();
    }
    else if (keyCode === LEFT_ARROW) {
        player.moveLeft();
    }
    else if (keyCode === ENTER){
        setup();
        spawnSky();
        player.show();
        checkCollision();
    }
}

function checkCollision() {
    let distance;
    for (let i = 0; i < sky.length; i++) {
        distance = dist(player.posX + player.size/4, player.posY + player.size/4, sky[i].posX, sky[i].posY);

        if (distance < 50-player.size/4) {
            noLoop();
            fill(255);
            text("slut", 720,height/2);
            console.log("detected");
        }
    }

}

