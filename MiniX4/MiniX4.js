function setup() {
  createCanvas(windowWidth,windowHeight);
  background(255);
  //Skriveboks 1
  Input = createInput("Skriv her...");
  Input.position(550,445);
  Input.size(400);
  //Skriveboks 2
  Input = createInput("Skriv her...");
  Input.position(550,545);
  Input.size(400);
  //Skrivboks 3
  Input = createInput("Skriv her...");
  Input.position(550,645);
  Input.size(400);

  //randomizing af hjerter
  for (let i = 0; i <35; i++){
    text("♥",random(width),random(height));
  }
}

function draw() {
  //Sort tekst
  fill(0);
  textSize(50);
  textFont("Georiga");
  text("Vi vil gerne forbedre din shopping oplevelse!",270,100);
  text("Giv os feedback!", 570,150);
  textSize(15);
  text("Tak for hjælpen :)",710,340)
  text("Hvordan var din shoppingoplevelse?:",645,440)
  text("Kunne du få besvaret dine eventuelle spørgsmål?:",610,540)
  text("Vil du shoppe her igen?",695,640);

  //Hvid tekst
  fill(255)
  textSize(50);
  textFont("Georiga");
  text("Vi vil gerne opsamle alle dine informationer!",275,200);
  text("Hjælp os med det!!", 570,250);
  textSize(15);
  text("Skriv venligst dit fulde navn:",675,480)
  text("Udfyld venligst med dine bankoplysninger:",635,580)
  text("Dit personnummer:",710,680)

  //øjne 1
  push();
  fill(255);
  noStroke();
  ellipse(100,200,50);
  ellipse(155,200,50);
  fill(0);
  textSize(15);
  text("♥",100,200);
  text("♥",155,200);
  pop();

  //øjne 2
  push();
  fill(255);
  noStroke();
  ellipse(300,400,50);
  ellipse(355,400,50);
  fill(0);
  textSize(15);
  text("♥",355,400);
  text("♥",300,400);
  pop();

  //øjne 3
  push();
  fill(255);
  noStroke();
  ellipse(100,600,50);
  ellipse(155,600,50);
  fill(0);
  textSize(15);
  text("♥",155,600);
  text("♥",100,600);
  pop();

  //øjne 4
  push();
  fill(255);
  noStroke();
  ellipse(1300,200,50);
  ellipse(1355,200,50);
  fill(0);
  textSize(15);
  text("♥",1340,200);
  text("♥",1285,200);
  pop();

  //øjne 5
  push();
  fill(255);
  noStroke();
  ellipse(1100,400,50);
  ellipse(1155,400,50);
  fill(0);
  textSize(15);
  text("♥",1140,400);
  text("♥",1085,400);
  pop();

  //øjne 6
  push();
  fill(255);
  noStroke();
  ellipse(1300,600,50);
  ellipse(1355,600,50);
  pop();
  fill(0);
  textSize(15);
  text("♥",1340,600);
  text("♥",1285,600);
  pop();
}
//farvelægning af baggrunden
function mouseMoved() {
  fill(0);
  ellipse(mouseX, mouseY, 100,200,100);
}

