## MiniX4 (The dark side)
Jeg har lavet et program som jeg har valgt at kalde "The dark side". Jeg har lavet et program, som er en kommentar på alt det mystiske ved dataindsamling. Personligt bliver jeg ofte overrasket når jeg finder ud af hvor mange informationer jeg giver ud. Facebook har eksempelvis adgang til alle mine statusopdateringer, billeder, minder, osv. Men hvad du måske ikke er klar over er at Facebook også har adgang til alt det du ikke har delt. Måske har du skrevet en statusopdatering, men fortrudt. Dette er informationer som Facebook beholder. Jeg synes ofte at hjemmesider og/eller spørgeskemaer ser meget uskyldige ud. Men hvilke informationer giver jeg egentlig adgang til? Selv når jeg har valgt at dele mine oplysninger, kan man stadig stole på det? Der findes to sider af alting. Selvom det ser meget uskyldigt ud kan det stadig være "farligt". Programmet skal være med til at skabe eftertanke hos modtageren. Nemlig at man skal være noget mere forsigtig omkring sine personlige oplysninger på nettet.

Jeg har lavet et interaktiv program. Programmet starter med at være en hvid skærm, som skal forestille et spørgeskema. Når brugeren så bevæger computer musen hen over skærmen vil baggrunden begynde at skifte farve. Når farven skiftes ses der en ny tekst, som er noget mere dyster end det uskyldige spørgeskema. Derudover er der også tilføjet øjne, så brugeren ville få en fornemmelse af at blive overvåget. Opgaven lød på, at man skulle eksperimentere med interaktive funktioner f.eks. mus, lyd, data capture. Jeg har valgt at basere mit program på funktionen `mouseMoved`. Dette er også den funktion, som er med til at ændre baggrundens farve. Jeg snublede egentlig over denne funktion inde på reference hjemmesiden, men endte med at få en ide ud fra det. Der var mange forskellige variationer af `mouseMoved`, men jeg valgte at bruge den i forbindelse med `fill`. Men brugeren kan også udfylde et felt med noget tekst. Denne del er selvfølgelig også interaktiv. Jeg fandt også denne funktion inde på reference hjemmesiden. 
````
function mouseMoved() {
  fill(0);
  ellipse(mouseX, mouseY, 100,200,100);
````
Jeg har valgt at funktionen skal danne en ellipse, som følger computermusen. Dette er beskrevet med den tredje linje i eksemplet. Derefter valgte jeg, at ellipsen skulle farvelægge baggrunden sort. Dette har jeg gjort ved brugen af `fill`, som vi kender meget godt. Jeg valgte at den egentlige baggrund på programmet skulle være hvid, da jeg på denne måde kunne lave en tydelig kontrast mellem "de to sider". Desuden kunne jeg "gemme" teksten i sort og hvid, da de jo udlignes i hinanden. Jeg har lært at lave en interaktivt program, hvor programmet kræver en interaktion fra brugeren. Derudover har jeg lært at forkorte nogle funktioner, hvor min source kode på den måde bliver mere enkel. Eksempelvis jeg har anvendt følgende loop til at placere nogle tilfældige hjerter. Jeg har været inde på en hjemmeside med en masse symboler, hvor et hjerte symbol. Her brugte jeg mit loop til at placere hjerterne ved brug af `text`-funktionen. Disse hjerte skulle egentlig placeret for at gemme de hjerte jeg har brugt til pupiller ved mine øjne. Da loopet skulle gentages en gang har jeg placeret det i `function setup()`. 

````
 for (let i = 0; i <35; i++){
    text("♥",random(width),random(height));
  }
````

Der er mange forskellige vinkler man kan vælge at lave denne opgave ud fra. Specielt når det er et meget bredt emne. Jeg blev klart inspireret meget af tidligere programmer, som jeg fandt under "Showcases". Ud fra dette dannede jeg en ide om hvad jeg gerne ville kommentere på. Jeg vil gerne skabe en ubehagelig følelse hos brugeren. Altså følelsen af at blive overvåget. Jeg tænkte nemlig et program skulle være provokerende for at kunne være tankevækkende. Programmet skal være nemlig med til at skabe eftertanke. Snakken har ofte handlet om at man skal være forsigtig på internettet. Men kan man overhovedet være forsigtig på internettet længere. Nogle gange er man ikke selv klar over hvad man takker ja til på internettet. 

Dette er også hvor jeg mener data capture har størst negativ indflydelse på samfundet. Når man ikke længere er klar over hvad bliver gemt, delt og/eller brugt. Kan man så overhovedet være en privatperson længere? Hvis man vil være en integreret del af samfundet kræver det ofte at man skal være aktiv på internettet. Det kan være sociale medier, men også at have adgang til en e-mail. Det er en forventning af samfundet, at hver enkelte borger befinder sig online. Så man er på en måde tvunget til at være online. Men så snart man opretter en profil, på en hvilken som helst applikation, har man så ikke allerede uddelt en masse informationer? Og er man klar over at man giver alle mulige adgang til sine informationer? Hvis ikke man er klar over det, kan man så sige at oplysningerne bliver stjålet? Det er meget vilde spørgsmål, men det er hvad programmet er lavet til. Programmet er ikke en løsning til et eventuelt program. Det skal ses som en oplysning.

#### Link til mit program
[Klik her for at køre min sketch](https://joan6375305.gitlab.io/aestetisk-programmering/MiniX4/index.html)

[Klik her for min source code](https://gitlab.com/joan6375305/aestetisk-programmering/-/blob/main/MiniX4/MiniX4.js)

### Billede af mit program
Det første billede er hvordan mit program ser ud når du åbner det:
![](hvid.png)
Det andet billede er hvordan det ser ud efter interaktionen:
![](sort.png)

### Litteraturliste
- [https://p5js.org/reference/](https://p5js.org/reference/)
- [https://coolsymbol.com/](https://coolsymbol.com/)
- [https://aesthetic-programming.net/pages/4-data-capture.html](https://aesthetic-programming.net/pages/4-data-capture.html)
- [https://archive.transmediale.de/content/call-for-works-2015](https://archive.transmediale.de/content/call-for-works-2015)
