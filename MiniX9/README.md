### MiniX9: E-lit (Gruppe)
[Klik her for at se Signes MiniX9](https://gitlab.com/signeursula/aestetisk-programmering/-/tree/main)

[Klik her for at se Marius MiniX9](https://gitlab.com/elmose-gruppe/aestetisk-programmering/-/tree/main)

[Klik her for at køre programmet](https://signeursula.gitlab.io/aestetisk-programmering/MiniX9/index.html)

[Source code MiniX9](https://gitlab.com/joan6375305/aestetisk-programmering/-/blob/main/MiniX9/MiniX9.js)

#### “I mørket stiger data op.”

Vi har valgt at lave et omskifteligt digt ud fra den kendte sang “I Østen Stiger Solen Op”. I forbindelse med at lave det omskifteligt har vi valgt, at alle navneord skal blive randomiseret gennem arrays fra en JSON-fil. Alle navneord er skiftet ud med et ord, som har tilsvarende stavelser til det originale ord. Det skal hjælpe med at gøre digtet nyt hver gang programmet genstartes, men stadig genkendeligt. Derudover har vi også valgt at inkludere de originale ord ved de oprindelige pladser. Vi synes, at det giver en spændende pointe, at der er en meget lille chance for, at den originale sang kan blive genereret. 

Grunden til at vi valgte “I Østen Stiger Solen Op” er fordi, at sangen er genkendelig. Selvom man udskifter nogle ord, vil sangen stadig kunne kendes af den almene dansker. Vi har også sørget for, at mange ord i en eller anden kapacitet har forbindelse med ‘Æstetisk Programmering’ og ‘Software Studies’. Dette ville vi gerne implementere for at skabe refleksioner hos modtageren. Eksempelvis “I mørket stiger virus op, den spreder krig på jord”. Her kommer vi til hele pointen og den diskussion man kan starte med vores MiniX. I forbindelse med øget teknologi og population på kloden, kan man reflektere og diskutere, hvorvidt vi når en grænse, hvor der kan skabes nyt indenfor vores kultur -digitalt eller ej. Der er diskussioner om Remix-culture, og hvorvidt vi virkeligt skaber noget nyt, eller bare remixer alt det gamle -for det er jo det som vores MiniX gør. Ydermere har nogle af de ord, som vi bruger, negative konnotationer tilknyttet til sig, det kan ende med at male dystre digte eller ‘bløde’, ‘rare’ digte. Det er et meget dystopisk perspektiv på den ellers meget idylliske sang.


### Hvordan fungerer programmet?

Programmet fungerer med sætninger, som bygger på konstanter. Vi har lavet to JSON-filer, som bestemmer værdien for disse konstanter. JSON-filerne er delt op i to på baggrund af de to forskellige vers. Inde i JSON-filen  `vers1` vil man finde de forskellige ord til første vers, som kan vælges imellem, når programmet autogenereres. Dette kan man også kalde for arrays. Det var et krav i opgavebeskrivelsen at have en eller flere JSON-filer med. Vi har valgt at dele den op efter vers, da det skabte mere orden i vores kode, da sætninger og konstanter skulle dannes, lidt ligesom de tidligere ‘classes’, som man også benytter til at skabe overblik, for ikke at lave for indviklet kode. 

Vores program er for det meste hardcoded. Vi ville have kort-skrevet og simplificeret vores kode, men vores fokus har ligget mere på den reflekterende side med denne MiniX. Vi har anvendt mange nye syntaks, og flere som vi også kendte førhen. Vi har anvendt nye syntaks såsom `math.floor` & `math.random`. Disse syntaks er ikke nogen vi har gjort os kendskab med før, men de hjælper med at generere programmets konstanter.

For at udfordre sanserne har vi valgt at tilføje en lydfil til koden. Det endte ikke med at fungere helt som vi gerne ville have. Vi ville gerne have at lydfilen spillede med det samme når programmet blev åbnet, dette kunne vi dog ikke få lov til grundet ‘AutoContext’ licensen som man kan finde i mange browsere. Dog fandt vi lidt en gylden middelvej i den her problematik. Det endte med at lydfilen godt ville afspille, når der blev trykket på vores knap “Mere poesi”, dog for hver gang man trykkede på knappen, så ville lydfilen afspilles igen og igen. Denne lyd har vi siden distorted, da vi syntes det var en spændende effekt, der hjalp med at drage endnu et dystopisk element over programmet, at jo mere ‘poesi’ man gerne vil have, desto mere forskruet og ulideligt bliver lyden. Dette er med til at give programmet denne ubehagelige stemning over sig, i søgen efter mere og mere nyt, der bare er mere og mere remixet, bliver lyden og stemningen mere og mere forskruet. 

### Hvad har vi lært?

Vi har lært lidt forskelligt af MiniX9. Vi har lært at anvende JSON-filer i forbindelse med en p5.js-fil. Det har givet os mere overskud, da det var noget nemmere at sortere i programmets mange linjer kode. Vi har fundet smarte løsninger til problemer vi stødte på undervejs, eksempelvis tilføjelsen af flere `sentence`, så der kom linje afbræk. Og sidst har vi reflekteret over hvordan den her Vocable Code kan udtrykkes i ens program, og vise de refleksioner man har bag programmet. Som der står i Aesthetic Programming: _"Vocable Code has a direct relation to bodily practices, the act of voicing something, and how the voice resonates with political practices"_ 
Vi mener at vores program fremviser det praktiske i brugen af JSON, og det reflektive ved Vocable Code, ved at tage noget poetisk genkendeligt og forvrænge det.

(Vi var desværre ikke i stand til at gøre selve koden til poesi i sig selv, det var lige ud over vores evne kraft i denne omgang)




### Billede af MiniX9
![](miniX9_screenshot.png)

