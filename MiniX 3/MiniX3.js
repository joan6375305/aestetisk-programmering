//rotation variabler
let vinkel = 0
let minusvinkel = 0

function setup() {
  //create a drawing canvas
  createCanvas(windowWidth, windowHeight);
  frameRate (30);
 }
 
 function draw() {
   background(70, 80);
   drawElements();

   //teksten som fremgår
   fill(0);
   textSize(20);
   textFont("Georgia");
   text("This is an atom.",675,150);
   text("We are each of us composed of atoms, but equally, we are composed by time.",410,600)

   push();
  //loop, som indeholder mine stjerner.
    fill(224,224,224,50);
  for (let i = 0; i <100; i++){
    //jeg har valgt at anvende random funktionen, så der ville skabes stjerner over det hele.
    text("*",random(width),random(height));
  }

      pop();
 }
 
 function drawElements() {
push();
translate(width/2, height/2);
noStroke();

push();
//blå - proton
rotate(vinkel);
fill(204, 229, 255);
ellipse(30,20,20,20);
vinkel+=10;
pop();

push();
//orange - neutron
rotate(minusvinkel);
fill(255, 229, 204);
ellipse(40,30,20,20);
minusvinkel-=20;
pop();

push();
//rød - elektron
rotate(vinkel);
fill(255, 204, 204);
ellipse(50,40,20,20);
vinkel+=10;
pop();

push();
//blå - proton
rotate(minusvinkel);
fill(204, 229, 255);
ellipse(60,50,20,20);
minusvinkel-=20;
pop();

push();
//orange - neutron
rotate(vinkel);
fill(255, 229, 204);
ellipse(70,60,20,20);
vinkel+=10;
pop();

push();
//rød - elektron
rotate(minusvinkel);
fill(255, 204, 204);
ellipse(80,70,20,20);
minusvinkel-=20;
pop();

   pop();
 }

 