## MiniX 3

##### Mit program
Jeg ville gerne lave et program, som kunne ligne noget. Jeg synes den kreative del var svær, da jeg var ret fikseret på den throbber som vi kender. Jeg synes nogle gange, at det kan være svært at tænke ud af boksen. Specielt når boksen er meget specifik. Derfor endte jeg med at lege rundt med mange forskellige måder at lave throbbers på. Jeg startede med at anvende den fremgangsmåde vi brugte første gang i undervisningen, men jeg endte ud i at anvende rotations funktioner i stedet. Dette var den nemmeste måde for mig at få mine ellipser til at rotere i forskellige retninger. Dette gjorde jeg ved brug af `minusvinkel`- og `vinkel`-variabler. På den måde synes jeg det endte med at ligne et atom, og arbejdede lige som min vej derfra. Jeg har lavet det så det ligner, at der er zoomed ind på et enkelt atom. Derudover har jeg valgt farver ud fra hvordan man typisk ville farvelægge et atom. I forhold til proton, neutron og elektron. Derudover synes jeg det var mega fedt at lave nogle atomer bagved. De små stjerner i baggrunden skal ihvertfald forestille en masse andre atomer. Til slut tilføjede jeg nemlig citatet, og jeg synes ud fra det at det fungere godt som en helhed. Jeg synes førhen at jeg har fået lavet nogle meget simple MiniX-afleveringer, men denne gang er jeg godt tilfreds. Jeg har gjort mig mange overvejelser i forhold til denne uges undervisning/emne. Jeg synes det er spændende at inddrage mine interresser i det som vi laver her. Jeg synes derfor jeg har fået udforsket og udtrykket det jeg egenlig gerne ville komme i mål med. Mit program ligner noget genkendeligt. Mit program er mere genemført end tidligere afleveringer. Det mener jeg i hvertfald selv. 

##### Min source kode
Jeg synes det kan være svært at inddrage tid ind i sin source kode. Jeg har haft meget forkus på at indkludere en throbber i mit program, og har på den måde ikke overvejet tid lige så meget. Jeg vil ikke sige jeg som sådan har nogle funktioner, som afhænger af tid. Det kan jeg se at nogle af de andre har inddraget, hvilket jeg vil gøre mere ud af næste gang. Jeg et loops, som på nogle parameter selvfølgelig har noget med tid at gøre. Det loop jeg har med skaber de stjerner som ses i baggrunden. Det er et meget simpelt loop fordi der ikke er behov for at være specifik. Placeringen af stjernerne er random, både i forhold til `windowWidth` og `windowHeight`. Da der ingen specifikationer er for placeringen af disse stjerner, bliver loppet meget simpelt. Jeg anvendte `text` til at lave min stjerner, og jeg brugte en lys grå i `fill` funktionen. Derudover har jeg lavet mange forklaringer til mig selv, som også kan ses i min source kode. 

````
   push();
  //loop, som indeholder mine stjerner.
    fill(224,224,224,50);
  for (let i = 0; i <100; i++){
    //jeg har valgt at anvende random funktionen, 
    så der ville skabes stjerner over det hele.
    text("*",random(width),random(height));
  }
      pop();
````
Grunden til det hele er random er programmets relation til atomer. Jeg synes også dette er et emne, som man kan perspektivere til tid. Det er også hvad citatet kommentere på. Men det er selvfølgelig ikke opgaven pointe. Men jeg mener stadig at programmet er en kommentar på tid. Når mit program åbnes, kan man se at tiden går. Man kan se tiden går fordi tingene bevæger sig. Hvis programmet have stoppet, ville tiden været stoppet fordi min source kode er konstant. Så længe som alt hardwaren virker, så bevæger mit program sig. Det er selvfølgelig en speciel måde at argumentere for det på. Men for ikke så lang tid siden anvendte man en throbber, som en slags indikation på tid. På den måde er mit program en indikation af tiden. Men også en indikation på at tingene fungere.

##### En throbber
Når jeg er stødt på en throbber ville det typisk være i de scenarier, som også nævnes. Men jeg synes der er forskel alligevel. På sociale medier ville det være en irritationsfaktor for mig at en throbber dukkede op. Det betyder at det ikke virker, og at jeg ikke kan komme til at lave det jeg gerne vil. Eksempelvis er der et billede, som ikke vil loade eller så kan jeg ikke sende en besked til min veninde. Her er en throbber en visualisering af en vist begrænsning, som er forekommet. Modsat hvis jeg er igang med at gennemføre en betaling. I det tilfælde synes jeg mere man venter uden irritationsfaktoren. Det er også på grund af throbberen at man ikke oplever irritationen. Hvis throbberen ikke var dukket op efter en transaktion ville man nok blive en anelse forvirret. Her er en throbber en visualisering af at noget fungere, men at der er lidt ventetid. Når man ved noget fungere, vil man ikke blive irriteret på samme måde. Det er en svær forskel at beskrive, men jeg mener at en throbber både kan ses, som noget negativ og positiv. Men jeg synes det er en spændende disskussion at have i forhold til ikoner. Ikoner er så alsidige at de kan have mange forskellige betydninger. Det kommer for det meste an på person, situation, tid og sted. Hvilket jeg finder meget interressant. 

### Link til mit program
[Klik her for at køre min sketch](https://joan6375305.gitlab.io/aestetisk-programmering/MiniX%203/index.html)

[Klik her for min source code](https://gitlab.com/joan6375305/aestetisk-programmering/-/blob/main/MiniX%203/MiniX3.js)

### Litteraturliste
[https://p5js.org/reference/](https://p5js.org/reference/)
[Link til mit citat](https://www.brainyquote.com/quotes/mohsin_hamid_878221?src=t_atoms)

### Billede af mit program
![](Minix3.png)
