## MiniX2 aflevering
Dette er min ReadMe til min miniX2. Den indeholder besvarelse på spørgsmålene, men også link til program, source kode, litteraturliste og billede af mit program. 

## Mit program
Jeg har lavet et program, hvor der kan ses på emojis. Det er meget basale emojis, da farverne her er det vigtigste. Jeg har gjort mig meget i, at de her emojis skulle være identiske. Det eneste, som skulle være forskellen mellem de emojis, var farverne. Jeg har brugt reference listen inde på [https://p5js.org/reference/](https://p5js.org/reference/). Her har jeg været inde under punktet med shapes, og brugt syntakser derfra. Jeg har brug to forskellige syntakser inde fra Shapes herunder `ellipse` og `arc`. Derudover skifter baggrunden farve, hvis du trykker ned. Jeg har kun inddraget det fordi jeg synes det ville være sejt. Det har ikke andet betydning for min kode. 

Da jeg har lavet to emojis, som er identiske er koden lang, men simpel. Det jeg ha brugt meget tid har igen været farverne. Jeg har machtet stroke med den mørke farve i emojien, og ellipse med den lyse farve. 
````
stroke(30,144,255);
strokeWeight(5);
fill(0,191,255);
ellipse(430,330,50);

fill(30,144,255);
ellipse(421,315,15);
ellipse(440,345,10);

fill(0,191,255);
ellipse(421,315,15);
````
Dette er et eksempel på hvor jeg har lavet forskellige farver til de forskellige `elllipse`. Koden er blevet lang fordi den gentager tingene to gange. Jeg har forsøgt på bedste vis at illustere med kommentar i source code, hvilken del af koden, der referere til hvert ansigt. Det er noget jeg forestiller mig, at jeg i fremtiden kan gøre på en smartere måde.

## Tanker og overvejelser
Min pointe med min MiniX2 er, at folk kun lægger mærke til forskelle hvis to forskellige pointer bliver præsenteret. Det måske en smule kringlet formuleret. Det jeg mener er, at forskellen ikke er vigtig. Jeg har lavet to næsten identiske emojis, hvor nogle vil forbinde det med køn grundet farverne. De fleste vil tænke jeg har lavet en dreng og pige emoji, da farverne er blå og lyserød. Men hvis jeg havde lavet en enkelt emoji eller valgt andre farver havde der ikke været et problem. 

Problemet opstår kun ved at jeg har præsenteret to køn, og derfor udeladt alt imellem. Emojis repræsentere kun noget, hvis brugere forbinder det med viden de selv har. Det er også noget jeg har tænkt en del over i forhold til disse snakke, vi har haft på klassen, om emojis. Hvis hudfarver til emojis aldrig havde været introduceret, havde der så været et problem. Måske skulle man lave emojis til nogle figurer, som beskriver følelser, men ikke repræsentere mennesker. Der vil selvfølgelig være mange, som ville være uenige med mig. Et eksempel jeg har er kysse-emojien. Der ses to mennesker kysse, den ene er en dreng den anden er en pige. Sådan startede det ihvertfald. Men siden hen er der tilføjet flere forskellige sammensætninger af køn, som kysser, for at repræsentere de forskellige seksualiteter, som findes. Eventuelt kunne man have lavet en kysse emoji, hvor hverken køn havde været repræsenteret. På den måde ville der bare være to mennesker, der kysser, frem for nogle blev krænket af det.

Når man tilføjer hudfarver til emojis, gør man det binært. Altså menneskeligt. Ud fra dette opstår problemet. Emojis, i min optik, er sjove at bruge fordi de forklare en følelse, som man ikke kan beskrive på tekst. For min skyld behøves min græde-emoji ikke at være min hudfarve. Modtageren af min besked forstår at jeg græder, som er hele pointen med beskeden. 

Men med at det sagt, synes jeg at denne debat om emojis har taget overhånd. Der er meget tale i medierne om, at forskellige mennesker skal blive repræsenteret for det de er. Men hvis man fjerner hele aspektet af at sætte mennesker i forskellige bokse, ville man måske også kunne eliminere denne diskussion. Dette bliver også talt om i den tildelte læsning til denne uge. Selvfølgelig er det vigtigt for nogle at blive repræsenteret i medierne, men jeg synes at emojis godt bare kan være emojis.

## Link til mit program
[Klik her for at køre min sketch](https://joan6375305.gitlab.io/aestetisk-programmering/MiniX%202/index.html)

[Klik her for min source code](https://gitlab.com/joan6375305/aestetisk-programmering/-/blob/main/MiniX%202/MiniX2.js)

## Litteraturliste
[https://p5js.org/reference/](https://p5js.org/reference/)

## Billede af mit program
![](MiniX2.png)
