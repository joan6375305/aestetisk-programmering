## MiniX8 (Gruppe og individuel)
[Klik her for at se Signes flowchart](https://gitlab.com/signeursula/aestetisk-programmering/-/tree/main/MiniX8)

[Klik her for at se Marius flowchart](https://gitlab.com/elmose-gruppe/aestetisk-programmering/-/tree/main/MiniX8)

[Flowchart 1 (Gruppe)](https://gitlab.com/joan6375305/aestetisk-programmering/-/blob/main/MiniX8/Voldtægtsmand_spillet_flowchart.png)

[Flowchart 2 (Gruppe)](https://gitlab.com/joan6375305/aestetisk-programmering/-/blob/main/MiniX8/Interresse_hjemmeside_flowchart.png)

Vi har lavet to flowcharts -inklusiv en selvstændig flowchart. De to flowcharts er idéer til den endelige MiniX11. Vi ville gerne have, at vores idé indeholdte et politisk aspekt, og derfor potentielt kan sætte nogle debatter i gang.

Vores første idé drager inspiration fra en UNICEF reklame, hvor der bliver fortalt, at afrikanske piger ofte bliver udsat for voldtægt. Dette er jo et tragisk emne, og vi ville godt lave et projekt som ville få folk til at handle og reflektere over, hvad der egentlig foregår rundt i verden. Vi kom på idéen om at lave et spil, hvor man styrer en afrikansk pige, som skal nå hjem og undgå voldtægtsmænd. Lidt alla ‘alle mine kyllinger kom hjem’. Vi tænkte så videre, om hvorvidt man kunne give spillet mere dimension ved at lave multiplayer. Vi tænkte derfra at tage inspiration fra South Park spillet, hvor sværhedsgraden er baseret på hudfarve. Dette er super racistisk, men det siger også meget om det virkelige samfund, hvor kvinder i f.eks Afrika i højere grad bliver ofre for voldtægt, end kvinder i Skandinavien. Vi tænkte på at have to spil (split screen) kørende samtidig på én computer, hvor det så er random om man er en pige fra Afrika eller en pige fra Nordeuropa. Man vinder ved at manøvrere sig hurtigt til målet, sit hjem, uden at støde på en voldtægtsmand. Kvinden på Afrika-siden vil opleve, at der er flere fjender (voldtægtsmænd) i deres spil, modsat den hvide pige. Dette kan virke absurd, men siger også noget om, hvordan kvinder fra samtlige kulturer er ofre for voldtægt, men at kvinder i afrikanske lande i højere grad er udsatte.

Vores andet projekt handler om, hvordan at dataindsamling kan føre til, at man ender i en indelukket bobbel, hvor man kun bliver fodret med mere og mere af samme holdning, og derfor kan ende i ekstremistiske grupper og selv få ekstremistiske holdninger grundet manglende imput fra folk med andre holdninger end din egen. Dette projekt skulle helt klart have til formål at være sjovt, samtidig med at kunne sætte nogle tanker i gang. Programmet fungerer således, at man starter på en "hjemmeside", som ligner et umiddelbar hub for fritidsinteresser i Århus. Der er så fem muligheder, hvorledes dén man vælger fører til ekstremistiske pop-up reklamer og anbefalede ekstremistiske grupper.

Når det kommer til flowcharts, var det klart lettest at lave vores første idé, da den er mere simpel. Der er kun to veje, der deler sig i spillet om manøvren i et overdrevent voldtægts præget miljø, med den afrikanske pige og den nordeuropæiske pige. Vores anden idé med ekstremistiske pop-ups var sværere at få formidlet i en flowchart, da der var fem forskellige muligheder. Dette gjorde det rent tegneteknisk svært med, hvordan at man lige skulle koble stregerne imellem de forskellige bokse.

Ved begge flowchart prøvede vi også at simplificere det. Vi bestræbede os efter eksempelvis at kunne give vores flowchart til nogen, som aldrig har hørt om idéen før, men stadig ville kunne forstå idéen ud fra skitsen.
Flowchartene er brugbar i den kontekst, at det giver et meget klart og simpelt overblik over, hvad projekterne præcis går ud på. Havde man bare læst en tekst (ligesom denne readme), kan det godt blive lidt uklart, hvordan programmet egentlig fungerer. Ved at lave et flowchart, kan man se projektets opbygning. Dog kan det også være meget tid at bruge, hvis vi ikke ender med at lave en idé som overhovedet har noget at gøre med disse to.

[Flowchart 3 (Individuel)](https://gitlab.com/joan6375305/aestetisk-programmering/-/blob/main/MiniX8/Flowchart3.png)

Spørgsmålene tilhørende denne MiniX8 er allerede beskrevet i ovenstående tekst. Her er spørgsmålene dog kun besvaret i forhold til gruppe delen af MiniX8. Jeg vil derfor give mit svar på spørgsmålene i forhold til den individuelle del af MiniX8. Jeg har taget samme tilgang igen til kompleksiteten af mit flowchart. Det skulle gerne kunne forståes af alle mennesker, også de som ikke har viden indenfor emnet. Derfor har jeg forsøgt at simplificere mit flowchart, og være meget generel i mine udtryk. Jeg kan se at den individuelle og gruppens flowchart kan hjælpe på mange forskellige måder. På den ene side kan jeg godt lide det overblik det skaber, at kunne beskrive sine ideer og koncepter med et billede. Da det til tider kan være svært at forklare et koncept, nemmere bare at kunne vise det. På den måde synes jeg også det skaber bedre kommunikation i gruppens, da man igen har bedre mulighed for at forklare hvad det er ens ide er. På den anden side er det også en mere tidskrævende fremgangsmåde fremfor andre metoder. Hvis det er et projekt hvor man ikke har fået givet meget tid, så ville jeg nok ikke hælde til at anvende et flowchart. Derudover sunes jeg at nogle af disse flowchart programmer er meget kompliceret. De går ofte i stå. 

### Billeder af flowchart
##### Gruppens første flowchart

![](Voldtægtsmand_spillet_flowchart.png)

##### Gruppens andet flowchart

![](Interresse_hjemmeside_flowchart.png)

##### Flowchart over MiniX6

![](Flowchart3.png)


