// Class fil til spiller 2 og deres bevægelse inden for spillet
class Spiller2 {
    constructor() {
        this.posX       = 945;
        this.posY       = 640;
        this.speed      = 20;
        this.sizeX      = 25;
        this.sizeY      = 52;
        this.direction  = "højre";
    }
    show () {
    push();
        imageMode(CENTER);
        if (this.direction === "højre") {
            image(player2højreImg, this.posX, this.posY, this.sizeX, this.sizeY);
        }  else if (this.direction === "venstre") {
            image(player2venstreImg, this.posX, this.posY, this.sizeX, this.sizeY);
        }
    pop();
    }

    moveLeft () {
        if (this.posX - this.speed >= 775) {
            this.posX -= this.speed;
            this.direction = "venstre";
        }
    }

    moveRight () {
        if (this.posX + this.speed <= 1115) {
            this.posX += this.speed;
            this.direction = "højre";
        }  
    }
}