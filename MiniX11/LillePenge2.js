// Class fil til penge-objekter for Spiller 2
class LillePenge2 {
  constructor() {
    this.posY = random(175, 185); // Start the ellipse above the canvas
    this.posX = random(780, 1030); // Random X position within the canvas width
    this.speed = random(2,3); // Random speed of falling
    this.sizeX = 38;
    this.sizeY = 35;
    this.image = loadImage("Billeder/penge1.png"); // Load the image
    this.remove = 3000;
  }

  move() {
    this.posY += this.speed; // Move the ellipse downwards

    // Check if the ellipse has reached the bottom of the canvas
    if (this.posY > 615 + this.sizeY) {
      this.reset(); // Reset the position and properties of the ellipse
    }
  }

  reset() {
    this.posY = random(175, 185); // Start the ellipse above the canvas
    this.posX = random(780, 1030); // Random X position within the canvas width
    this.speed = random(2,3); // Random speed of falling
    this.size = 30; // Random size of the ellipse
  }

  show() {
    image(this.image, this.posX, this.posY, this.sizeX, this.sizeY); // Display the image
  }

  removed() {
    this.posX = this.remove;
    }
}