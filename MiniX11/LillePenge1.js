class LillePenge1 {
  constructor() {
    this.posY = random(155, 165);
    this.posX = random(370, 650);
    this.speed = random(2,3);
    this.sizeX = 38;
    this.sizeY = 35;
    this.image = loadImage("Billeder/penge1.png"); 
    this.remove = 3000;
  }

  move() {
    this.posY += this.speed;

    if (this.posY > 615 + this.sizeY) {
      this.reset();
    }
  }

  reset() {
    this.posY = random(155, 165);
    this.posX = random(370, 660);
    this.speed = random(2, 3);
    this.size = 30;
  }

  show() {
    image(this.image, this.posX, this.posY, this.sizeX, this.sizeY); 
  }

  removed() {
  this.posX = this.remove;
  }
}