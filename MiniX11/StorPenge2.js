class StorPenge2 {
  constructor() {
    this.posY = random(175, 185); 
    this.posX = random(780, 1100); 
    this.speed = random(1, 1); 
    this.sizeX = 29;
    this.sizeY = 49;
    this.image = loadImage("Billeder/penge3.png");
    this.remove = 3000;
  }

  move() {
    this.posY += this.speed; 

    if (this.posY > 590 + this.sizeY) {
      this.reset(); 
    }
  }

  reset() {
    this.posY = random(175, 185); 
    this.posX = random(780, 1100); 
    this.speed = random(1, 1); 
  }

  show() {
    image(this.image, this.posX, this.posY, this.sizeX, this.sizeY); 
  }

  removed() {
    this.posX = this.remove;
    }
}