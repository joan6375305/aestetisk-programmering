## MiniX11 Draft
[Klik her for at se Signes MiniX11](https://gitlab.com/signeursula/aestetisk-programmering/-/tree/main)

[Klik her for at se Marius MiniX11](https://gitlab.com/elmose-gruppe/aestetisk-programmering/-/tree/main)

[Draft](https://gitlab.com/joan6375305/aestetisk-programmering/-/blob/main/MiniX11/Draft.pdf)

[Eksamens projektet](https://joan6375305.gitlab.io/aestetisk-programmering/MiniX11/index.html)

[Eksamensprojektet opgaven](https://gitlab.com/joan6375305/aestetisk-programmering/-/blob/main/MiniX11/Eksamensprojektet.pdf)
