class MellemPenge2 {
    constructor() {
      this.posY = random(175, 185); 
      this.posX = random(780, 1030); 
      this.speed = random(2, 3); 
      this.sizeX = 37;
      this.sizeY = 39;
      this.image = loadImage("Billeder/penge2.png");
      this.remove = 3000; 
    }
  
    move() {
      this.posY += this.speed; 
  
      if (this.posY > 612 + this.sizeY) {
        this.reset(); 
      }
    }
  
    reset() {
      this.posY = random(175, 185); 
      this.posX = random(780, 1030); 
      this.speed = random(2, 3); 
      this.size = 30; 
    }
  
    show() {
        image(this.image, this.posX, this.posY, this.sizeX, this.sizeY); 
    }

    removed() {
      this.posX = this.remove;
      }
  }