class StorPenge1 {
    constructor() {
      this.posY = random(175, 185); // Start the ellipse above the canvas
      this.posX = random(370, 660); // Random X position within the canvas width
      this.speed = random(1, 1); // Random speed of falling
      this.sizeX = 29;
      this.sizeY = 49;
      this.image = loadImage("Billeder/penge3.png"); // Load the image
      this.remove = 3000;
    }
  
    move() {
      this.posY += this.speed; // Move the ellipse downwards
  
      // Check if the ellipse has reached the bottom of the canvas
      if (this.posY > 590 + this.sizeY) {
        this.reset(); // Reset the position and properties of the ellipse
      }
    }
  
    reset() {
      this.posY = random(175, 185); // Start the ellipse above the canvas
      this.posX = random(370, 660); // Random X position within the canvas width
      this.speed = random(1, 1); // Random speed of falling
    }
  
    show() {
        image(this.image, this.posX, this.posY, this.sizeX, this.sizeY); // Display the image
    }

    removed() {
      this.posX = this.remove;
      }
  }