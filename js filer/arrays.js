let frugter = [];
frugter [0] = "æble";
frugter [1] = "pære";
frugter [2] = "banan";
frugter [3] = "melon";

//kan også skrive let frugter = ["æble", "banan", "melon"];
console.log(frugter[2]);
console.log(frugter.length);

function setup() {
  // put setup code here
  createCanvas(windowWidth,windowHeight);
  background(255);

}

function draw() {
for(let i=0; i<frugter.length; i++){
text(frugter[i], width/2, height/2);
 
}}
function windowResized(){
  resizeCanvas(windowWidth,windowHeight);
}