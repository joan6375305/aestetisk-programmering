## MiniX10 (Style-Transfer) (Marius, Signe Ursula og Joan)
[Klik her for at se Signes GitLab](https://gitlab.com/signeursula/aestetisk-programmering/-/tree/main)

[Klik her for at se Marius GitLab](https://gitlab.com/elmose-gruppe/aestetisk-programmering/-/tree/main)

#### Hvilken eksempel kode har I valgt, og hvorfor?
Vi har valgt at tage Style-Transfer, fordi debatten om plagiat og stil-tyveri, inden for digital kunst, fylder meget i medierne inden for de seneste måneder, især eskaleret af AI’s eksplosive udvikling. Så det ville være spændende at dykke ned i en algoritme, hvor man kan undersøge, hvordan maskinen lærer fra billederne i algoritmen og oversætter den stilart til et andet motiv.

#### Har I ændret noget i koden for at forstå programmet? Hvilke?
Sketch.js koden som er tilknyttet Style Transfer er meget ligetil. Det er en kort og komprimeret kode, så det var ikke svært at enten ødelægge den eller manipulere den ved bare at ændre på få linjer. Den er inddelt i en del forskellige filer, som er hhv. Javascript, JSON og HTML. Det eneste der virkelig kunne hjælpe med at ændre på koden for at forstå programmet var at tilføje console.log til at forstå JSON filerne’s brug af variabler.
En enkelt pointe er dog, at selvom det meste var ligetil, så blev vi dog forvirret når vi gik ind i en `variable` fil, da der enten opstår nogle tegn, som vi ikke kan genkende, eller dukker der intet op.
![](Kode1.png)

#### Hvilke linjer af kode har været særlig interessante for gruppen? Hvorfor?
Vi synes følgende billede viser den mest interessante linje kode, som er i StyleTransfer:
![](Kode2.png)

Den her linje samler tidligere defineret style1 (der er også en lignende kode for style2) og bygger ovenpå den vha. transfer. Her bliver inputIMG, og en function med ‘err’ og ‘result’ brugt til at skabe et billede ovenpå et andet indenfor de givne parametre. Der bliver brugt DOM elementer, i form af ‘parent’, som dertil også refererer til ‘StyleA’, som man kan finde i index.html.

#### Hvordan ville I udforske/udnytte begrænsningerne af eksempelkoden eller machine learning algoritmer?
Ved denne kode findes der forskellige begrænsninger. For at lave vores egne ændringer på koden har vi indsat vores egne billeder, som vi har fundet på internettet. En begrænsning som var her var at billederne skulle have størrelsen 300x300. Det gør at mange billeder ikke er mulige inde ved denne kode. Derudover findes der andre begrænsninger i forhold til at man kun kan anvende billeder og ikke videoer. Derudover er der kun en enkelt funktion ved denne specifikke kode. Ved machine learning algorithms støder man ofte på at der kun findes en enkelt funktion. Det kunne være spændende hvis man kunne ændre på konceptet lidt og få videoer til at kunne ændre udseende også eller ændre udseendet på andre ting. 

#### Var der syntakser/funktioner, som I ikke kendte til før? Hvilke? Og hvad lærte I?
Brugen af DOM elementer, samlet med HTML defineret kode, inde i Javascript var noget af en omgang at vikle hovedet om, som vist og fortalt om i den førnævnte style1.transfer. Hele den linje kode gik ud på at overføre machine learning variablerne for et originalt billede, til det billede man nu havde sat ind. Det er hjertet af koden som sådan. Fra det lærte vi hvordan man kunne forbinde alle de forskellige elementer i programmet, fra de forskellige linjer kode.

#### Hvordan kan I se en større relation mellem eksempel koden, som I har undersøgt, og brugen af machine learning ude i verdenen (fx. kreative AI, Voice Assistance, selvkørende biler, bots, ansigtsgenkendelse osv.)?
Vi har undersøgt en kode, hvor den tager et eksempel og ændrer stilen derpå ud fra den viden den bliver givet. Dette kan man relatere til DALL-E 2. Det er en kreativ AI indenfor OpenAI. Her kan man også give inputs og derefter vil der blive genereret et eksempel på hvad du lige har bedt om. Disse to ting minder meget om hinanden, men på andre planer minder StyleTransfer om mange forskellige AI på basal plan. Koden forklarer meget godt hvordan AI fungerer. Der er et input, der er nogle krav og derefter bliver der givet et resultat inden for de ting kriterier. Det synes vi også helt klart er en af de ting, som er mest spændende ved at have valgt denne kode. 

#### Hvad kunne du tænke dig at vide mere om? Eller hvilke spørgsmål kan du ellers formulere ift. emnet?
Nu hvor vi har arbejdet med generering af stilarter indenfor kunst, kunne det være interessant at vide, hvad der skal til for at AI kan komme op med sin helt egen stilart. et andet spørgsmål man også kan spørge sig selv om er, om det overhovedet er muligt at komme på “nye” stilarter. Når vi nogensinde har en grænse, hvor det ikke er muligt at finde nye stilarter, uden at det i forvejen er set før i kunstens verden? Der er mange kunstnere, som har meget ens stilarter, hvor man næsten ikke kan se forskellen, selvom at begge stilarter bærer præg af kunstnernes egen stil og æstetik. Kopiere ikke-AI (altså rigtige mennesker) ikke også andres stilarter? Er kunstnere ikke oftest inspireret af andre kunstneres værker?

![](Tele1.png)
![](Tele2.png)
![](Tele3.png)


