# MiniX 1
Dette er en ReadMe til min første MiniX aflevering.

## Hvad har jeg lavet?
Jeg har lavet en sketch, som minder mig om noget jeg selv har set før på en hjemmeside. Specifikt har jeg lavet en sketch, hvor det ligner at en hjemmeside loader. Jeg havde ikke mange ideer til hvad jeg kunne lave, men jeg så en syntax (`saveGif()`), som gav mig ideen.

Det har været noget frustrerende at forsøge at programmere selv. Ofte stødte jeg ind i problemer som jeg ikke selv mente, at jeg kunne løse. Jeg har fået meget hjælp fra de andre studerende og instruktører. Jeg er stødt ind i virkelig mange problemer, som stoppede min process undervejs. Det har nok været en af de mest tidskrævende opgaver jeg længe har lavet. Ihvertfald i forhold til størrelsen af opgaven. Det har været en process hvor man skal være meget tålmodig, og forsøge at holde fokus på trods af frustrationerne. Det begyndte at give mening med placeringen af former, i forhold til de tre akser. Derfor synes jeg det var meget sjovt at forsøge at placere en rektangel, som passede præcis med min tekst. Derudover var det tilfredsstillende at skifte farverne på de forskellige ting, og lege lidt rundt med det. Herfra kunne jeg også vurdere hvad jeg synes der var pænest i forhold til farve sammenspillet. Da det har utrolig mange gentagelser, følte jeg at jeg hurtigere blev bedre og bedre. Når man havde fået en tekst til at virke før, kunne jeg godt gøre det igen. Så selvom det har været en meget frusterende process, har den været meget spændende. Jeg har haft lyst til at løse de problemer, som opstod. Det var bare ikke hver gang, at jeg følte jeg havde den viden endnu til at få det løst. Det er noget jeg forventer jeg får i løbet af dette semester. 

Jeg har brugt mange forskellige syntax inde fra [p5js](https://p5js.org/reference/). Jeg forsøgte mange forskellige eksempler indtil jeg kom frem til det endelige resultat. I det endelige resultat har jeg anvendt flere forskellige syntax. Jeg har som sagt anvendt `saveGif()`, men jeg har også indsat former og tekst. Jeg har anvendt `rect()`, `circle()` og `text()`. Derudover har jeg anvendt andre syntax til at manipluere selve formerne, som blev indsat. Her har jeg brugt `fill()`, for at skifte farverne på diverse ting. Jeg har blandt andet også brugt `textSize()` og `noStroke()` til at bestemme udseende på formerne. Jeg har brugt meget tid på at kopiere de syntax, som ligger tilgængeligt. Efter det har lykkes mig at efterligne de syntax, som er, har jeg modificeret dem efter mit formål. På den måde kunne jeg eksempelvis placere cirklerne efter mine ønsker. Derudover bestemte jeg også hvor hurtigt de bevægede sig og fraven på cirklerne. 

Der har været mange udfordringer ved at lære at programmere. Det er som at lære et nyt sprog, og det kan derfor virke meget intimiderende. Der er mange nye termer og redskaber at lære. Derfor synes jeg det er svært at sætte ord på de forskelle og ligheder, som findes ved at skrive og programmere. En lighed er at det er begge nye ting, som jeg synes minder meget om hinanden helt praktisk. Det er en ny måde at skabe på, men også meget interessant. Den helt klare forskel for mig er at det er noget mere kompliceret at programmere. Når man skriver varierer det ikke så meget fra det man normalvis vil være vant til. Programmering, for mig, er et helt nyt univers hvor man på en anden vis kommer bag om facaden. Der er mange flere nye komponenter til programmering modsat at skrive. 

Programmering er blevet et nyt sprog, som flere og flere kender til. Jeg tror at fremtiden vil blive præget meget af programmering, og den måde at tænke på. Programmering giver flere muligheder for hvad man kan fremstille, og derfor tror jeg det bliver mere og mere aktuelt. Teksterne har også været med til at give en dybere forståelse for det behov, som findes for programmering. Hvor jeg førhen ikke kunne forstå vigtigheden af programmering, men på mange måder er det rygraden for vores moderne samfund.

En ting jeg har lagt meget mærke til allerede nu er, at vi lærer utrolig meget fra hinanden. Det er spændende at se andres ideer, og deres måde at gribe opgaven an på. Der findes utrolige mange forskellige veje til det samme resultat. Det er derfor virkelig dejligt at tale med de andre studerende om opgaven og programmeringen. Det er nemt at få tips fra hinanden, og på den måde bliver vi hele tiden bedre. Det er selvfølgelig meget at sige her på første uge, men jeg synes vi har gjort os meget i at hjælpe hinanden. Jeg udvikler mine kompetencer ved at hjælpe min medstuderende, og omvendt.


## Litteraturliste
[https://p5js.org/reference/](https://p5js.org/reference/)


## Sketch
[Klik her for at køre min sketch](https://joan6375305.gitlab.io/aestetisk-programmering/MiniX%201/index.html)

![](miniX1.png)
