## MiniX 7 (Genbesøg MiniX)
##### MiniX2
Jeg har valgt at genbesøge MiniX2 (Geometric emoji). Det har jeg valgt fordi det er en af de af de første opgaver vi stødte på i dette semester. Jeg synes derfor der var mange ting jeg kunne forbedre eller tilføje til programmet. Derudover synes jeg at emnet er meget spændende, og noget jeg sidenhen har gjort mig mange tanker omkring. Jeg synes derfor det var oplagt at jeg valgte at genbesøge MiniX2. 

***Jeg har lavet flere forskellige ændringer til mit program, men for at sætte det i perspektiv har jeg igen her givet adgang til min orginale MiniX2 (Program, source code og billede).***

[Orginale MiniX2](https://joan6375305.gitlab.io/aestetisk-programmering/MiniX%202/index.html)

[Source code: Orginal MiniX2](https://gitlab.com/joan6375305/aestetisk-programmering/-/blob/main/MiniX%202/MiniX2.js)

![](MiniX2.png)

##### Hvilke ændringer har du lavet og hvorfor?
Jeg har lavet nogle forskellige ændringer, som viste nogle forskellie slags forbedringer. Jeg tog udgangspunkt i MiniX2 fordi jeg tænkte vi nok hvae lært en masse siden da. Jeg havde, i starte, derfor meget fokus på forbedringer, fremfor ændringer. Jeg brugte meget til på at forkorte den orginale kode. I miniX2 havde jeg sværtt ved at se hvor mange linjer kode jeg skulle gentage for at programmet ville fungere. Ofte da noget fungerede besluttede je mig for at beholde det, fremfor at forsøge mig frem med andre muligheder. Det var ofte fordi jeg var taknemlig for at det virkede. Dette kan jeg se har ændret sig meget. Jeg har forkortet koden rigtig meget, og dermed givet meget mere overblik i koden. Alle unødvendige linjer er stortset fjernet. Det er en klar forbedring, som jeg har lavet.

Det er gået fra at være følgende:
````
//ansigt 1 (venstre)
stroke(30,144,255);
strokeWeight(5);
fill(135,206,250);
ellipse(500,height/2,300);

//øjne 1
//venstre
stroke(30,144,255);
strokeWeight(5);
fill(0,191,255);
ellipse(430,330,50);

fill(30,144,255);
ellipse(421,315,15);
ellipse(440,345,10);

fill(0,191,255);
ellipse(421,315,15);

//højre
stroke(30,144,255);
strokeWeight(5);
fill(0,191,255);
ellipse(560,330,50);

fill(30,144,255);
ellipse(551,315,15);
ellipse(570,345,10);

fill(0,191,255);
ellipse(551,315,15);

//mund 1
stroke(30,144,255);
strokeWeight(5);
fill(0,191,255);
arc(500,420,80,80,0,PI,CHORD);
````

Til at være dette:
````
//ansigt 1 (venstre)
stroke(0,0,153);
strokeWeight(5);
fill(0,0,255);
ellipse(200,height/2,300);

//øjne 1
fill(0,0,204);
ellipse(130,330,50);
ellipse(121,315,15);
ellipse(140,345,10);
ellipse(121,315,15);
ellipse(260,330,50);
ellipse(251,315,15);
ellipse(270,345,10);
ellipse(251,315,15);
`````

Jeg ville gerne beholde programmets budskab, og jeg ville gerne lave ændringer, som egentlig bare var forbedringer æestetisk. Derfor har jeg taget rigtig meget inspiration fra den orginale MiniX2. Da jeg første gang sad med MiniX2 havde jeg en klar forestilling om, at jeg ville kommentere på mennesket og de forbindelser/koblinger vi selv skaber. Derfra de problemer, som skabes derfra, er typisk ikke særlig vigtige i min optik. Specielt det her med emojis, det mener jeg stadig ikke er en vildt vigtig debat. Jeg ser stadig en meget simpel løsning til det. Men ihvertfald, dette ville jeg sætte mere fokus på. Derfor har jeg valg at tilføje teksten "What gener are we?", for at specificere hvad min pointe med opgaven jo egentlug var. 

[MiniX7](https://joan6375305.gitlab.io/aestetisk-programmering/MiniX7/index.html)

[Source code: MiniX7](https://gitlab.com/joan6375305/aestetisk-programmering/-/blob/main/MiniX7/MiniX7.js)

![](miniX7.png)

##### Hvad har du lært i denne miniX? Hvordan kunne du inkorporere eller videreudvikle koncepterne i din ReadMe/RunMe baseret på litteraturen vi har haft indtil nu?
I denne MiniX har jeg lært hvor meget vi egentlig har lært. Jeg er blevet meget mere klar over hvor meget undervisningen over de seneste måneder egentlig har gjort. Der er virkelig stor forskel på hvordan jeg ville takle MiniX2 for en måned siden, og hvordan jeg ville takle det nu. Jeg vil sige det har meget at gøre både med den viden vi har gjort os inden for ÆP og SS, men specielt også vores viden til at anvende værktøjerne. I starten var det meget svært overhovedet at få et program til nogenlunde at fungere. Derfor fokuserede man mere på den del, end man fokuserede på ReadMe-delen. Mnu har man nemllig overskud/overblik til at fokusere mere på selve opgavem, fremfor at lege med koden. Derfor gør man sig måske også flere overvejelser før man begynder på opgaven. JEg gjorde mig bestemt flest overvejelser i forhold til alt det holdundervisning, som vi har haft. Jeg synes det var cool hvis jeg anvendte et `loop`, fordi det kunne jeg virkelig ikke forstå hvordan virkede da vi lavede MiniX2. Jeg ville gerne have noget tekst, som satte emnet i fokus, fordi jeg gerne ville skabe endnu mere fokus på mit budskab. 

##### Hvad er relationen mellem æstetisk programmering og digital kultur? Hvordan demonstrere dit værk perspektiver i æstetisk programmeing (henvend evt. til forordet i grundbogen)?
Sådan som jeg ser det, bruges æstetisk programmering, som et værktøj til at kommentere på den digitale kultur. Den digitale kultur er noget som hele tiden ændre sig, og takler flere og flere emner. Det er også noget som bliver talt om i forhordet i grundbogen. Den digitale kultur har no-limits, og inkludere derfor rigtig mange emner. Det er et værktøj, som enhver kan bruge, hvor der derved skabes mange forskellige perspektiver. Mit værk demonstere mit perspektiv indenfor dette specifikke emne. Jeg har brugt mine krafter på at formidle det budskab jeg finder mest interresant indenfor dette emne. Nemlig at der er visse andre ting, som jeg mener, er en vigtigere diskussion end inclusitivity in emojis. 
