function setup() {
  // put setup code here
createCanvas(windowWidth,windowHeight);
background(255,218,185);
}

function draw() {
  // put drawing code here

  //ansigt 1 (venstre)
stroke(30,144,255);
strokeWeight(5);
fill(135,206,250);
ellipse(500,height/2,300);

//øjne 1
//venstre
stroke(30,144,255);
strokeWeight(5);
fill(0,191,255);
ellipse(430,330,50);

fill(30,144,255);
ellipse(421,315,15);
ellipse(440,345,10);

fill(0,191,255);
ellipse(421,315,15);

//højre
stroke(30,144,255);
strokeWeight(5);
fill(0,191,255);
ellipse(560,330,50);

fill(30,144,255);
ellipse(551,315,15);
ellipse(570,345,10);

fill(0,191,255);
ellipse(551,315,15);

//mund 1
stroke(30,144,255);
strokeWeight(5);
fill(0,191,255);
arc(500,420,80,80,0,PI,CHORD);

//ansigt 2 (højre)
stroke(255,0,127);
strokeWeight(5);
fill(255,153,204);
ellipse(1000,height/2,300);

//øjne 2
//venstre
stroke(255,20,147);
strokeWeight(5);
fill(255,105,180);
ellipse(930,330,50);

fill(255,20,147);
ellipse(920,315,10);
ellipse(940,345,10);

fill(255,105,180);
ellipse(921,315,15);

//højre
stroke(255,20,147);
strokeWeight(5);
fill(255,105,180);
ellipse(1060,330,50);

fill(255,20,147);
ellipse(1050,315,10);
ellipse(1070,345,10);

fill(255,105,180);
ellipse(1051,315,15);

//mund 1
stroke(255,20,147);
strokeWeight(5);
fill(255,105,180);
arc(1000,420,80,80,0,PI,CHORD);
}

//baggrund
function mousePressed(){
  background(random(100,200),random(100,200),random(100,200));
}