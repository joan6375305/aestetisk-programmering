var g;
var b;
var a;

let symbol = "☺";
let symbolX = 50;
let symbolV = 50;
let afstand = 70;

function setup() {
createCanvas(windowWidth,windowHeight);
background(0);
frameRate(1);
}

function draw() {
  r = random(255); 
  g = random(100,200); 
  b = random(100); 
  a = (255); 
  for (let i = -3; i <100; i++){
    fill(0);
    textSize(50);
    strokeWeight(4);
    for (let j = 0; j < 2; j++){
      stroke(r,b,a);
      text(symbol,symbolX+(afstand*i),symbolV+(afstand*j))
      for (let j = 2; j < 4; j++){
        stroke(r,b,a,80);
        text(symbol,symbolX+(afstand*i),symbolV+(afstand*j))
        for (let j = 4; j < 6; j++){
          stroke(r,b,a,30);
          text(symbol,symbolX+(afstand*i),symbolV+(afstand*j))
          for (let j = 6; j < 8; j++){
            stroke(r,b,a,9);
            text(symbol,symbolX+(afstand*i),symbolV+(afstand*j))
            for (let j = 8; j < 10; j++){
              stroke(r,b,a,2);
              text(symbol,symbolX+(afstand*i),symbolV+(afstand*j))
          }
        }
      }
    }}}
    noStroke(0);
  fill(r,b,a);
  textSize(75);
  text('What gender are we?', 400, 90);
  fill(r,b,a,80);
  textSize(75);
  text('What gender are we?', 400, 225);
  fill(r,b,a,60);
  textSize(75);
  text('What gender are we?', 400, 360);
  fill(r,b,a,40);
  textSize(75);
  text('What gender are we?', 400, 495);
  fill(r,b,a,20);
  textSize(75);
  text('What gender are we?', 400, 640);

//ansigt 1 (venstre)
stroke(0,0,153);
strokeWeight(5);
fill(0,0,255);
ellipse(200,height/2,300);

//øjne 1
fill(0,0,204);
ellipse(130,330,50);
ellipse(121,315,15);
ellipse(140,345,10);
ellipse(121,315,15);
ellipse(260,330,50);
ellipse(251,315,15);
ellipse(270,345,10);
ellipse(251,315,15);

//mund 1
arc(200,420,80,80,0,PI,CHORD);

//ansigt 2 (højre)
stroke(153,0,153);
strokeWeight(5);
fill(255,0,255);
ellipse(1300,height/2,300);

//øjne 2
fill(204,0,204);
ellipse(1230,330,50);
ellipse(1220,315,10);
ellipse(1240,345,10);
ellipse(1221,315,15);
ellipse(1360,330,50);
ellipse(1350,315,10);
ellipse(1370,345,10);
ellipse(1351,315,15);

//mund 2
arc(1300,420,80,80,0,PI,CHORD);
}
