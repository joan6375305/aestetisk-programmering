## MiniX5 (SKIFTER FARVE HURTIGT)

Mit program er skabt af to hoveddele. Et loop og en if statement, som også står i opgavebeskrivelsen. Jeg har lavet et program hvor der autogenereres en baggrund og små ellipser foran. Både baggrund og ellipser skifter farver efter random funktionen, som blev henvist til i opgavebeskrivelsen. Reglerne er at ellipserne dannes på vilkårlige steder indenfor en ramme. Baggrund er skifter farve, og "genstarter" programmet baseret på frameCount. Programmet er meget kort og hurtigt. Det starter forfra hele tiden, derfor sker der heller ikke noget anderledes når der er gået lang tid. Jeg synes det har været svært at forstå termet emergent behavior, og dermed svært at relatere det til min egen opgave. Men sådan som jeg forstår det har jeg lavet nogle regler, som derfra skaber nogle uskrevne "regler". Da mit program er meget kort, og går meget hurtigt, kan det være en smule svært at beskrive den her emergent behavior. Men man kan antage at baggrundsfarven aldrig vil være ens lige efter hinanden. Hvis programmet havde været langsommere havde det været nemmere at observere disse emergent behavior. Måske kan man også antage at ellipserne ikke ville dannes lige ved siden af hinanden med det samme. Men da programmet har den hastighed, som det har, ville det ikke være noget man tænker over.

Mit program består af et `for loop` og en `if statement`. Min `if statement` bestemmer baggrundsfarven baseret på `frameCount`. 
```
function draw() {
  if (frameCount %60 == 0) {
    background(random(255),random(255),random(255));
  }
```
Mit `for loop` generere mange `ellipse` inden for `windowHeight, windowWidth`, men placaeringen herinde er `random`. 
````
  for (let i = 0; i <300; i++){
    noStroke();
    fill(random(255),random(255),random(255),100);
    ellipse(random(width),random(height),20);
````
#### Reglernes roller
Selvom der virkelig kun er to hovedregler i mit program spiller de en stor rolle for hvordan programmet bliver. Man kan måske også argumentere for at de får en større betydning grundet antallet af regler. Med færre regler jo større betydning får de enkelte regler. Jeg brugte lang tid på at lege rundt med programmet, og så den store forskel på hvad små ændring gjorde. Jeg startede med at lave et loop hvor ellipserne var større, hvor det påvirkede programmets hurtighed. Da der var større ellipser opfattede man programmet til at gå langsommere end det egentlig var. Jeg prøvede mig også frem med frameCount, og hvor høj en procent if-statementet skulle påvirkes af. Her ændrede jeg direkte på programmets hurtighed, i forhold til hvor ofte det skulle "genstartes". Her var det vigtigt for mig, at man kunne se et mønster i programmet, før det startede forfra. Det kan godt være at programmet stadig havde besvaret på opgavebeskrivelsen, hvis frameCount var anderledes. Men der ville man ikke kunne se emergent behavior eller de typiske andre mønstre som kan se på nuværende tidspunkt. På den måde kan få regler have stor betydning for et program.

#### Ugens læsning
Vi fik to citater i instruktør timerne og jeg har valgt at tage udgangspunkt i det ene citat. **“As Katie Salen and Eric Zimmerman put it, uncertainty ‘is a key component of meaningful play’ [...] Once the outcome of a game is known, the game becomes meaningless.”** (Montfort et al., 2012). Dette kan man også bruge i relation til mit program. De første to eller tre gange man ser mit program, virker det spændende, men måske også som om der kommer til at ske noget. Når man så har set det, og kan se at der ikke sker noget nyt bliver det meningsløst og uinteressant. Dette er også hvad citatet kommentere på. For når udkommet af noget er kendt, bliver det dermed meningsløst. Hvilket også er det jeg har forstået mest ved auto-generator. Det er noget vi ikke lægger mærke til, og noget som ofte føles ligegyldigt. Man kan ikke ændre på det, derfor tænker man ikke så meget over det. Der vil selvfølgelig ofte være behov for auto-generators, men da vi ikke kan påvirke dem, er de ikke vigtigere for mennesket. De er mere vigtige for selve programmets funktion. 

#### Link til mit program
[Klik her for at køre min sketch](https://joan6375305.gitlab.io/aestetisk-programmering/MiniX5/index.html)

[Klik her for min source code](https://gitlab.com/joan6375305/aestetisk-programmering/-/blob/main/MiniX5/MiniX5.js)

### Billeder af mit program
![](MiniX5.png)
![](MiniX55.png)

### Litteraturliste
- [https://p5js.org/reference/](https://p5js.org/reference/)

